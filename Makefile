CC ?= clang
CXX ?= clang++
CXXFLAGS = -std=c++14 -O2 -g
CFLAGS ?= -O2 -g
ex01: ex01.cpp
	$(CXX) $(CFLAGS) ex01.cpp -o ex01
